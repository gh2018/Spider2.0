package com.leadingsoft.common.model;

import org.beetl.sql.core.annotatoin.AssignID;

public class HotelComment {
	private Long commentId;
	private String hotelId;
	private Integer commentNum;
	private Integer commentPage;
	private String commentContent;

	@AssignID
	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId == null ? null : hotelId.trim();
	}
	public Integer getCommentNum() {
		return commentNum;
	}
	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}
	public Integer getCommentPage() {
		return commentPage;
	}
	public void setCommentPage(Integer commentPage) {
		this.commentPage = commentPage;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent == null ? null : commentContent.trim();
	}
}
